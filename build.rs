#[path = "src/structs.rs"]
mod structs;

use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::Path;

fn main() {
    let path = Path::new("codegen.rs");
    let db = File::open("resources/fi.inflectional.v1.tsv").unwrap();
    let buffer = BufReader::new(db);

    let mut file = BufWriter::new(File::create(&path).unwrap());
    let mut builder = phf_codegen::Map::new();

    for line in buffer.lines() {
        // let sana = Sana::new(&line);
        // builder.entry(sana);
        builder.entry("test", &line.unwrap());
    }
    writeln!(
        &mut file,
        "static DB: phf::Map<&'static str, &'static str> = \n{};\n",
        // "static DB: phf::Map<&'static str, Sana> = \n{:?};\n",
        // for line in buffer.lines() {
        //     let sana = Sana::new(&line);
        //     builder.entry(sana);
        // }
        builder.build()
    )
    .unwrap();
}
