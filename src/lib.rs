mod structs;
use anyhow::{bail, Result};
use std::fs::File;
use std::io::{BufRead, BufReader};
use structs::*;

// const ASIAKIRJA: &str = "resources/fi.inflectional.v1.tsv";

include!("../codegen.rs");

fn import_words(path: &str) -> Result<Vec<Sana>> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    reader.lines().map(|l| Sana::new(&l.unwrap())).collect()
}
