use anyhow::{bail, Result};
use std::fs::File;
use std::io::{BufRead, BufReader};

const ASIAKIRJA: &str = "resources/fi.inflectional.v1.tsv";

#[derive(Debug, PartialEq)]
pub enum Sana {
    Verb(Verb),
    Noun(Noun),
    Adjective(Adjective),
}

#[derive(Debug, PartialEq)]
pub struct Verb {
    base_word: String,
    trg_word: String,
    features: String,
    suffix: String,
    src_word: String,
}

#[derive(Debug, PartialEq)]
pub struct Noun {
    base_word: String,
    trg_word: String,
    features: String,
    suffix: String,
}

#[derive(Debug, PartialEq)]
pub struct Adjective {
    base_word: String,
    trg_word: String,
    features: String,
    suffix: String,
}

impl Sana {
    pub fn new(string: &str) -> Result<Self> {
        let split = string.split('\t');
        let sana: Vec<&str> = split.collect();
        if sana[2].starts_with("V;") {
            return Ok(Sana::Verb(Verb {
                base_word: sana[0].to_string(),
                trg_word: sana[1].to_string(),
                features: sana[2].to_string(),
                suffix: sana[3].to_string(),
                src_word: sana[4].to_string(),
            }));
        } else if sana[2].starts_with("N;") {
            return Ok(Sana::Noun(Noun {
                base_word: sana[0].to_string(),
                trg_word: sana[1].to_string(),
                features: sana[2].to_string(),
                suffix: sana[3].to_string(),
            }));
        } else if sana[2].starts_with("ADJ;") {
            return Ok(Sana::Adjective(Adjective {
                base_word: sana[0].to_string(),
                trg_word: sana[1].to_string(),
                features: sana[2].to_string(),
                suffix: sana[3].to_string(),
            }));
        } else {
            bail!("Parsing error")
        }
    }
}
