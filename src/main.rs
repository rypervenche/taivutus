use anyhow::{bail, Result};
use std::fs::File;
use std::io::{BufRead, BufReader};

const ASIAKIRJA: &str = "resources/fi.inflectional.v1.tsv";

fn main() -> Result<()> {
    let sanat = import_words(ASIAKIRJA)?;
    println!("Sanat: {:#?}", sanat);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT_GOOD: &str = "resources/test.txt";

    #[test]
    fn import() {
        let output_good = Sana::Verb(Verb {
            base_word: "muodostaa".to_string(),
            trg_word: "muodostan".to_string(),
            features: "V;ACT;PRS;POS;IND;1;SG".to_string(),
            suffix: "n".to_string(),
            src_word: "muodosta".to_string(),
        });
        let output = import_words(INPUT_GOOD).unwrap();
        assert_eq!(output_good, output[0]);
    }
}
